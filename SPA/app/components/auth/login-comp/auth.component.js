/**
 * Created by amurav on 29.03.2017.
 */
import {AuthController} from './auth.controller'

export const authComp = {
    templateUrl: './components/auth/login-comp/auth.html',
    controllerAs: 'vm',
    controller: AuthController
};
