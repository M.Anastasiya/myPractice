/**
 * Created by amurav on 23.03.2017.
 */

import {AccountController} from './account.controller';

export const accountComp = {
    templateUrl:'components/account-comp/account.template.html',
    controller: AccountController,
    controllerAs: 'vm'
};