/**
 * Created by amurav on 31.03.2017.
 */
export const serviceStatistic = () => {
    return {
        getActivityStatistic: () => {},
        getUploadStatistic: () => {},
        getQuantityStatistic: () => {},
        getActivityStatisticByUser: (currentUser) => {},
        getUploadStatisticByUser: (currentUser) => {},
        getQuantityStatisticByUser: (currentUser) => {}
    }

};